package bj.samples.dagger2

import javax.inject.Inject

class Service2 @Inject constructor(private val dependency: Service){
    init {
        println("I'm service 2!")
    }

    fun show() { dependency.show() }
}