package bj.samples.dagger2

import javax.inject.Inject

class Client {

    lateinit var service: Service2


    @Inject
    fun injectService(target: Service2 ) {
        service = target
    }

    fun show() {
        service.show()
    }
}