package bj.samples.dagger2

import javax.inject.Inject

class Service @Inject constructor(){
    init {
        println("hello world")
    }

    fun show() { println("show message") }
}
