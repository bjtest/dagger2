package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import dagger.Component

@Component
interface Injector {
    fun inject(client: Client): Client
}