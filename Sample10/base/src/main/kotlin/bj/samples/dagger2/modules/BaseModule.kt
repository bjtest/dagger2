package bj.samples.dagger2.modules

import bj.samples.dagger2.Service1
import bj.samples.dagger2.Service2
import bj.samples.dagger2.ServiceInterface
import bj.samples.dagger2.qualifiers.ClassQualifier
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
abstract class BaseModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideService1(): Service1 = Service1()

        @Provides
        @JvmStatic
        fun provideService2(): Service2 = Service2()

    }

    @ClassQualifier(Service1::class)
    @Binds
    abstract fun provideService1Imp( serviceImp: Service1) : ServiceInterface

    @ClassQualifier(Service2::class)
    @Binds
    abstract fun provideService2Imp( serviceImp: Service2) : ServiceInterface

}