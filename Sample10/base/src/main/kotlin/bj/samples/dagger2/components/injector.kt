package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.Service1
import bj.samples.dagger2.Service2
import bj.samples.dagger2.ServiceInterface
import bj.samples.dagger2.modules.BaseModule
import bj.samples.dagger2.qualifiers.ClassQualifier
import dagger.Component

@Component(modules = [BaseModule::class])
interface Injector {

    @ClassQualifier(Service1::class)
    fun getService1() : ServiceInterface

    @ClassQualifier(Service2::class)
    fun getService2() : ServiceInterface

    fun inject(client: Client): Client

}