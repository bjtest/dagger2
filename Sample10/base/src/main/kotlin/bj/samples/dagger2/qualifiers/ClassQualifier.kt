package bj.samples.dagger2.qualifiers

import javax.inject.Qualifier
import kotlin.reflect.KClass

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ClassQualifier(val classType: KClass<*>){
}
