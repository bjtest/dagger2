package bj.samples.dagger2

import bj.samples.dagger2.qualifiers.ClassQualifier
import javax.inject.Inject
import javax.inject.Named

class Client {

    @ClassQualifier(Service1::class)
    @Inject
    lateinit var service1: ServiceInterface

    @ClassQualifier(Service2::class)
    @Inject
    lateinit var service2: ServiceInterface

    fun show() { service1.show(); service2.show() }
}