package bj.samples.dagger2

import bj.samples.dagger2.components.DaggerInjector


fun main(args: Array<String>) {

    val client = Client()

    val injector = DaggerInjector.create()

    injector.inject(client).show()

    injector.getService1().show()

    injector.getService2().show()

}