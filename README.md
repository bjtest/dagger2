## dagger2

### why

通常第一個要問的就是我們為什麼要用他?通常你找資料他就是會很簡短的跟你說因為是要做到dependency injection(之後簡稱DI),所以才用他,但不能再多說一點嗎?

#### dependency injection

DI相關的有除了來自wiki的心得外另外就是來自[這篇文章](https://blog.csdn.net/anmei1912/article/details/101614327?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.nonecase)

要討論DI,我們不得不先知道dependency,另外我們會多提一下兩個東西,就是DIP(dependency inversion priciple)和IOC(inversion of crontrol),前者在研究clean architecture的部分已經有所涉略,可以看一下wiki裡面有關[DIP](https://en.wikipedia.org/wiki/Dependency_inversion_principle)和[IOC](https://en.wikipedia.org/wiki/Inversion_of_control),不過我還是稍微提一下.DIP和DI有相關但是卻不太一樣,當作補充就好了,IOC的其中一種方法就是DI,也是稍微看一下,不過DI建議一定要清楚,這對dagger2的一些用法會有很大的提示,畢竟文件爛到炸

##### dependency

假設我們有兩個classA,B我們說A depend B,在下面幾個狀況

1. A有一個屬性是B type,例如

   ```
   class A {
   	var attr : B? = null
   }
   ```

2. A使用B的方法,例如

   ```
   class A() {
   	val objB = B()
   	fun dependB() { objB.member() }
   }
   ```

3. A的method有參數是B type,或是return B type,例如

   ```
   class A {
   	fun dependB() : B { ... }
   }
   ```

4. A繼承自B

   ```
   class A : B {...}
   ```

這樣做的問題在於,當你要使用A的時候,無可避免會需要B,這會帶來幾個問題

* 當你改動B,就會影響到A,甚至一層一層更深遠
* 當你在組合模組的時候會因為他們之前的關聯變得更複雜,比方說如果A需要B的instance,那你可能需要先instance B
* 你如果讓兩個模組相關聯,你就更難重複利用,或是測試,比方說如果一個下載的模組,他無可避免會有和網路硬體相關的部分,如果他針對android有一個網路的模組做關聯,那當你要用這個下載模組和ios結合的時候,有可能你就沒辦法重複利用,或是測試的時候因為你內嵌了android的模組,就會變得一次得測試好幾個模組.

##### DIP,IOC,DI

由於依賴無可避免,所以我們要想一些方法,來讓上面說這些依賴的壞處可以被去除,IOC,DIP就是其中相關的設計理念,目的是要降低dependency帶來的壞處.

我們用前面例子舉例,現在有個模組為下載,我們有一個跟android network相關的模組,讓他可以在android device下下載東西,我們先直觀的設計

```
class AndroidNetwork {
	fun download(url: String) : String { ... }
}

class DownloadData {
	val android = AndroidNetwork()
	fun download(url: String) : String = android.download(url)
}
```

DownloadData是一個下載模組,AndroidNetwork是一個實作模組,這樣寫法有很多壞處,首先假如你改動了AndroidNetwork,讓他download return是其他格式,你會牽連到DownloadData,另外你如果要把DownloadData換平台,需要擴充DownloadData,或是改寫DownloadData,測試也是很麻煩,DownloadData的測試會很麻煩,你需要AndroidNetwork也要測試,而AndroidNetwork很可能很大一包,很難測以外,他還牽扯到硬體,這很糟糕,你可能還需要實體機器和環境才能完整測試.另外你這種寫法也不能使用unit test裡面的mock和stub

那我們稍微改寫一下

```
class DownloadData(val android : AndroidNetwork) {
	fun download(url: String) : String = android.download(url)
}
```

恩,他好多了,現在產生android object的責任已經不在DownloadData上了,兩個的關聯少了一層,變成從外部放進來,這東西其實就是IOC的一環

IOC的意思就是反轉控制權,這個反轉到底反哪個?反的是傳統的procedural programming,傳統裡面我們寫一些function,可能就是自己call當lib用,但是IOC的重點就是現在你寫的東西不是給你自己用,是給系統用,其實就是現在已經很普遍的event driven方式的東西.傳統我們寫一個程式可能自己負責一切,但是現在都是靠api跟framework溝通,你寫的東西已經不是你在用,而是系統在用.

但是這跟上面這些東西有甚麼關係?

主要是因為IOC裡面,東西都是dynamic binding,因為是系統要拿來用的,不是static決定的,所以IOC裡面的幾個方法就是runtime dynamic binding要注意的準則.而其中一個IOC的技巧,就是上面那個改寫法,而它的名稱就叫DI

DI分為client(客戶)和service(服務),以上面的例子來看,DownloadData是客戶,客戶需要一個服務就是AndroidNetwork,但是我們不由客戶產生這個服務,而是由外部(injector)提供客戶你要的服務.他可以說是IOC裡面廣泛使用的技巧.就跟外包一樣,所以客戶不需要管你外包商怎麼搞定這個服務,你只要把東西給我就好了.我不需要自己產生一個部門來做這個服務,或是應付這個部門裡面的愛恨情仇.

它的好處在於你現在其實降低了service和client的關聯,比方說如果這個service的create還需要更多的參數,那你在client端是不是也要傳進去這些參數才能製作service,現在不用了,那是外面(injector)的事情,client變得更單純,他只和實際有使用service的部分做關聯,其他地方無關.所以client也變精簡,code變少,你現在測試也會容易多了,可以用mock或是stub取代掉service,測試會變簡單.

DI裡面,把service的產生交由上層決定而非客戶端本身,所以他反轉了你對service的控制,將他給出去了.但是實務上的做法裡面,很多DI framework裡面其實service或是client的生成都是藉由injector,然後injector再幫client把依賴注入,除了因為很多client本身也可能是別人的service外,最大原因還是因為client需要注入service才能正常運作,所以很多時候injector也包辦client的產生,才能在一開始的時候就注入service.但是並不是絕對,至少一開始的注入點,很多時候你沒有那個object就沒辦法開始了.

好吧如果要講dagger2或許我們可以到此為止,但是還沒講到另一個dip.沒空可以跳過.

dip裡面包含兩件事

1. High-level modules should not depend on low-level modules. Both should depend on abstractions (e.g. interfaces).
2. Abstractions should not depend on details. Details (concrete implementations) should depend on abstractions.

我想第一眼完全不曉得他在講啥,之前看clean architecture那邊就是很好的一個用DIP設計的概念

high,low這邊就有點像前面講的A,B一樣,或是service,client,high代表比較大的,複雜的,就是client,low可能是幾個小零件的功能,像是service

我直接照上面講的把他改寫,應該就能馬上抓到他的意思.

```
interface DownloadInterface {
	fun download(url: String) : String
}
class AndroidNetwork : Downloadinterface {
	override fun download(url: String) : String { ... }
}
class DownloadData(val download : DownloadInterface ) {
	fun download(url: String) : String = download.download(url)
}
```

仔細看應該可以發現重點了,那就是其實DownloadData裡面完全沒有提到android,所以不管怎樣,DownloadData以後不管到哪個平台你都可以直接拿來用,他已經和你怎麼去implement無關了,你之後可能會寫新的IosNetwork繼承DownloadInterface,但是和DownloadData無關.

不管是client或是service,都只和abstraction(interface)有關聯,他兩者無實際的關聯,client使用DI將interface注入,service implemnt interface.

interface我們一開始學的時候就是把它當作一個節省的工具,同樣性質的method在不同的implement,使用interface,有點輕量化的base class,但是他這邊的用法就默默地把dependency拆解到只剩下跟一個abstract的method有關聯了,其他都無關.這裡也有可能是一個abstract的class,重點在於abstraction,裡面並沒實體內容,只是空殼,你到時候可以被任意替換,測試上也方便,不會有相關性.

這些做法能把原先依賴的兩者拆解,變成他們各自可以獨立存活,對client你可以增加重複可用性,也可以增加測試度,可以帶來很多好處

#### types of DI

一般來說DI主要就是injector要怎麼把service注入client,injector大概有三種方式來達成注入

1. constructor injection:injector利用client constructor把service放進去,其實就是我們上面舉的例子,它是DI最建議的做法

   ```
   class client(val service: Service)
   ```

   因為他在construct階段就把service放入,所以代表service在之後就可以馬上使用不會有問題,不過他也缺乏你在中間更換service的靈活性,但是他immutable的特性也可以確保thread safe

2. field/setter injection:它比較不同的是injector是藉由setter或是直接修改field將service寫進去

   ```
   class client {
   	lateinit var service : Service
   }
   ```


   直接寫入field就是public field,如果是非public可以提供一個setter function來寫入,這方法就是保有靈活度,你可以中途修改,或是你無法動到constructor,比方android的activity你就沒辦法動到.它的問題在於你injector搞不好的話,當你需要service結果他還沒注入就會有null問題了.

3. interface injection:這方法其實跟上一個乍看下分不太出來,也就是他的service注入是藉由一個interface,而client繼承這個interface

   ```
   interface ServiceSetter {
   	fun setService(service: Service)
   }
   
   class client : ServiceSetter {
   	lateinit var service : Service
   	override fun setService( in: Service ) { service = in }
   }
   ```

   它的用途在於如果你無法干涉到client,可以藉由擴充的方式來加掛service,另外由於client都有implement interface,所以對injector來說client都是同一種type,injector可以收集client對它們做群體的操作.

#### what can dagger2 do?

雖然前面講很多,但是我們重點是在講dagger2,不過至少我們知道了一點背景知識.dagger2可以做到的其實在於前面提到的DI,IOC裡面的一項技術.DI我們會不斷提到三個角色,client(依賴者),service(被依賴的),injector(將前兩者結合的),dagger2當然主要扮演的就是injector的角色了.很多DI framework有不同的方法來生成還有注入service,dagger2的方式是藉由annoation processor的方法,也就是註解在compile time的時候生成code來處理,它的好處在於它非runtime生成,有些DI framework還會使用reflection之類的方法,但是我們知道reflection對效能是種傷害.不過他也有些缺點,因為是compile time決定,所以其實他對type是相當嚴格,所以對於OO的多型,在它上面會比較麻煩,不是那麼靈活.很多東西可能要做到比較懂,所以我們先儘快來看一下實作.

#### 小結論

1. dagger2是一個DI(dependency injection)的工具
2. DI主要分為三個東西,client,service,injector,client使用service但是不生成service,而是injector生成service然後注入client,client再使用
3. dagger2主要扮演的角色為幫你生成injector,它是藉由annotaion processor來做到這件事,就是註解compile time生成code

### fiest dagger2 program

先開始啟用第一個小程式,我們還是以android studio為主要IDE,我這邊用project架構可以參考[sop文章](sop#not so pure kotlin project),code在dagger2/Sample1

#### setting

首先當然要加入dependency,在對應模組的build.gradle下

java的話使用下面的dependency

```
    api 'com.google.dagger:dagger:2.x'
    annotationProcessor 'com.google.dagger:dagger-compiler:2.x'
```

x是目前最新的版本,請用[maven central](https://search.maven.org/artifact/com.google.dagger/dagger)查一下,不要傻傻填x

kotlin的話不能用annotation processor,要使用kapt

```
	api 'com.google.dagger:dagger:2.x'
    kapt 'com.google.dagger:dagger-compiler:2.x'
```

前面記得加入plugin

```
apply plugin: 'kotlin-kapt'
```

#### simple sample

我們前面提到DI有三個東西client,service,injector,dagger2主要就是要產生一個injector來注入service至client,我們要先稍微提一下兩個註解,它是@Inject還有@Component,不多說直接看例子

kotlin版本

```
class Service @Inject constructor() {}

@Component
interface Injector {
	fun inject(client: Client)
}
```

雖然我都用kotlin寫範例來練習,但是這邊特別放進去java版本

```
class Service {
	@Inject
	Service() {}
}

@Component
interface Injector {
	void inject(Client client);
}
```

我想這應該就是dagger2弄DI最基本的型態,我前面講過三個東西,client,service,injector,首先service,dagger2裡面,service需要在他的constructor加上@Inject的註解,然後我們需要一個Injector,Injector做法就是@Component,Injector必須是一個interface,然後我們現在使用最陽春的做法,就是定義一個pure virtual function,名稱不重要,它的參數為Client的type,它的回傳,我上面用的是沒有回傳的版本,實際上他可以回傳,他回傳的就是client本身.例如下面的作法.

```
@Component
interface Injector {
	fun inject(client: Client) : Client
}
```

這樣看起來好像完了,但是其實還要做一點事,就是關於client,client要怎麼注入service

```
//kotlin
class Client {
    @Inject
    lateinit var service: Service
}
```

```
//java
class Client {
	@Inject
	Service service;
}
```

你可以看到@Inject放在field/property上面,這代表到時候Injector會幫你把service注入到這邊.所以這三個就是DI裡面的三個東西,client,service,injector,接著我們繼續看main,下面就只用kotlin了.

```
fun main(args: Array<String>) {

    val client = Client()

    DaggerInjector.create().inject(client)

}
```

在main裡面你看到突然多一個沒看過東西.這邊要注意一下,當你完成componet和inject之後,請先build一次,在build/classes/java底下會產生很多code.dagger2是java的你用kotlin不要去kotlin目錄找是永遠不會找到的,由於code會生成,所以當你有所改動的時候,clean之後再build或是就rebuild的動作很重要,如果他沒生成,上面main的例子你使用新增的class就會有找不到的問題.

我們剛剛用了compoent註解在一個interface上面,他會產生一個對應的class,名稱就是Dagger前綴加上你interface的名稱,我們這裡例子就會是DaggerInjector,他就是我們的Injector

##### under the hood

我們先稍微往下挖一下,首先先來看的是最單純的service那邊.當你將一個class使用@Inject註釋它的constructor,他就會被當作一個service,build之後會產生一個class,貼在下面

```
public final class Service_Factory implements Factory<Service> {
  @Override
  public Service get() {
    return newInstance();
  }

  public static Service_Factory create() {
    return InstanceHolder.INSTANCE;
  }

  public static Service newInstance() {
    return new Service();
  }

  private static final class InstanceHolder {
    private static final Service_Factory INSTANCE = new Service_Factory();
  }
}
```

它是純java的,總之就是產生一個service name後綴_Factory的class,顧名思義就是產生service的factory,他是一個singleton,裡面也很簡單,就是產生factory還有產生service,就兩個單純的功能,我想產生service這是我們要注目的點.

接下來我們先把client的@Inject拿掉,我們先看看單純的@Component會產生甚麼東西.

```
public final class DaggerInjector implements Injector {
  private DaggerInjector() {

  }

  public static Builder builder() {
    return new Builder();
  }

  public static Injector create() {
    return new Builder().build();
  }

  @Override
  public void inject(Client client) {
  }

  public static final class Builder {
    private Builder() {
    }

    public Injector build() {
      return new DaggerInjector();
    }
  }
}
```

上面是DaggerInjector的樣子,可以看到它有包含builder,create,另外就是我們剛剛interface定義的inject method,由於我們把client inject拿掉他現在也沒有甚麼東西要注入的,所以你可以看到它其實甚麼事都沒做.我們可以用DaggerInjector的create來create injector,或是你要用builder().build(),也可以

那現在我們把client加上inject,然後我們把component的method改成return client看看.

編譯完後我們可以發現client加上之後又多產生一個東西,我們來看看.

```
public final class Client_MembersInjector implements MembersInjector<Client> {
  private final Provider<Service> serviceProvider;

  public Client_MembersInjector(Provider<Service> serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

  public static MembersInjector<Client> create(Provider<Service> serviceProvider) {
    return new Client_MembersInjector(serviceProvider);}

  @Override
  public void injectMembers(Client instance) {
    injectService(instance, serviceProvider.get());
  }

  @InjectedFieldSignature("bj.samples.dagger2.Client.service")
  public static void injectService(Client instance, Service service) {
    instance.service = service;
  }
}
```

這東西看起來就複雜了一點,他是一個memberInjector,它的constructor裡面有設定一個provider,就是serviceProvider,還有一個static create,injectMember是一個interface的繼承,它實際底下implement委派到這個class自己的injectService,他裡面做的事情很簡單就是把給service塞進去field,而service的來源是由provider提供的,其實還蠻單純的,值得注意的是你可以觀察到一件事就是field/propery的存取,它是直接對instacne塞的,所以這代表@Inject指定的field/property必須要是public的,不然你塞不進去.

那我們回過來看新的component變成啥樣子了,由於改動的地方只有inject method,為了專心我只節錄這個method和它相關的

```
  @Override
  public Client inject(Client client) {
    return injectClient(client);}

  private Client injectClient(Client instance) {
    Client_MembersInjector.injectService(instance, new Service());
    return instance;
  }
```

首先這個inject他也委派它的任務給自己一個新method為injectClient,因為我們加了return client,它就直接回傳injectClient,看樣子如果是void,它就只是不回傳而已.

那我們看看injectClient做了啥事,他利用了剛剛我們看的Client_MembersInjector,而且他很凶狠不演了,直接跳到最後一步直接使用injectService這個static,他也不要Client_MembersInjector的實體了,直接就產生一個service塞進去.

##### result

所以這個範例做了甚麼事,我們create一個DaggerInjector,然後呼叫inject,她就幫我們把service塞到client裡面去.這就是我們前面講的DI做的事情,injector把service塞到client裡面,client不插手於service的生成.

##### limitation

從這個小例子我們開始dagger第一步,但是我們也從這個例子看到一些限制

* 使用field/property injection你必須要是public的field/property,不然你沒辦法做

* service加入的方法是針對constructor做@Inject註解,但是我們不是每次都能動到constructor,如果遇到這種狀況下怎麼辦?

* 類似上面,如果我有一個service interface就沒辦法.因為interface沒有constructor

* 如果service有其他的依賴有辦法嗎?

* 從剛剛的source裡面看到,他只要產生source的時候就會new一個新的,但是如果我們希望能每次都傳同一個讓大家共用有辦法嗎?

* field/property的部分,如果他是一個interface,那我要怎麼把它注入?

* 同上,如果是interface就會牽扯到多型,我能夠指定想要的implement type給特定的field/property嗎?

  

接下來我們會一一針對上面的這些問題提供更多的方法來解決

#### 小結論

我們使用一個最簡單的dagger2範例

1. service的部分,我們在class constructor加上@Inject就可以
2. client要被inject的filed/property,我們在對應的field/property上加上@Inject
3. @Component是製作一個injector的方法,針對一個interface註解@Component,這個interface裡面可以有一個method,參數為client,回傳可為client或是void
4. @Component會產生一個Dagger前綴你自定義interface name的class,提供create()還有你定義的inject method可以使用

### @Inject

我們前面提了一些限制,後面會慢慢解決它們,不過因為東西很多,有的也很複雜,我打算先就最不複雜的註解就是@Inject,來快速掃過它的功能,這部分的範例code在dagger2/Sample2

其實@Inject用的地方很簡單,它只有用在三種地方,其中兩種我們前面已經看過了,哈哈

1. 註解在constructor上.當你註解在constructor上,它就代表這是一個service,所以會產生service factory來產生service,在dagger的爛文件裡面,他把這種行為稱作add to graph,其實就是injector它會維護一張圖,那張圖就是各個service的產生,所以當你這麼做,就等於讓graph有產生新service的能力,我們等一下會針對它多講一些

2. 註解在field/property上,這代表他是要被inject的目標,field要為public,這前面有提過

3. 註解在method上面.dagger文件之所以爛就是在這邊可見一斑,針對這種作法,我引述文件中對這種作法的描述

   ```
   Dagger also supports method injection, though constructor or field injection are typically preferred.
   ```

   然後就沒了,哈哈哈.所以他到底是為什麼要註解在method,你註解在method會發生甚麼事?它完全沒提到,真的垃圾,沒關係不懂我們就做實驗,等一下[method injection](#method injection)的部分會講

#### constructor injection

我們這邊稍微多探討一些特別的狀況,Service加入graph,是dagger2裡最基礎的一件事,唯一的方法就是藉由constructor設定inject,然而很多時候事情並沒有那麼單純,要是service constructor有dependency呢?我們來製作一個service2

```
class Service2 @Inject constructor(private val dependency: Service){
    init {
        println("I'm service 2!")
    }
}
```

可以看到,這就是前面DI說的constructor injection,直接從constructor將service塞進去,這次的client變成service2,我們來看一下產生碼

```
public final class Service2_Factory implements Factory<Service2> {
        private final Provider<Service> dependencyProvider;

        public Service2_Factory(Provider<Service> dependencyProvider) {
                this.dependencyProvider = dependencyProvider;
        }

        public Service2 get() {
                return newInstance((Service)this.dependencyProvider.get());
        }

        public static Service2_Factory create(Provider<Service> dependencyProvider) {
                return new Service2_Factory(dependencyProvider);
        }

        public static Service2 newInstance(Service dependency) {
                return new Service2(dependency);
        }
}
```

你可以看到它的改變不大,重點是產生instance的method變成帶一個dependency進來,另外有一個get,get的dependency來源是provider.之前沒有dependency版本的factory裡面並沒有provider,而他現在變成你要create這個factory還需要額外的provider了.我們現在將原來的client改成inject Service2,看看injector的變化.

```
        private Service2 getService2() {
                return new Service2(new Service());
        }

        public Client inject(Client client) {
                return this.injectClient(client);
        }

        private Client injectClient(Client instance) {
                Client_MembersInjector.injectService2(instance, this.getService2());
                return instance;
        }
```

節錄如上,你可以看到很有趣的點,他這次的getService2(),他幫你把service給塞進去了

實際上這就是dagger2另一個特別的地方,當你services進去graph裡面,他們之間如果互相有相依性,它會幫你解決掉,而且你也不用額外設甚麼,如果他已經在graph裡面,就代表它能幫你生成,而當你需要某個service,它的dependency也會藉由graph幫你補齊,你不需要額外製造,另外你也可以看到,當如果client,service兩者都在graph裡面,你還可以用constructor injection,不像field一樣還會限制你要public,再加上前面提到的自動幫你生成,這就是dagger2的優點,另外如果service之間彼此有相依性,它也會幫你把順序給弄好,你不會搞錯順序.

所以很多人使用dagger2,大概就是你能做到的東西都塞到graph去,然後讓他幫你處理new的問題.

#### method injection

我們改寫client,接續上面例子,如果跳過來看可以先不用管Service2,就另外一個Service

```
class Client {
    lateinit var service: Service2

    @Inject
    fun injectService(target: Service2) {
        service = target
    }
 }
```

還記得我們前面講DI嗎?其實是我們inject是有用setter function做的,或是interface之類的,總之就是藉由method去inject,而非直接處理field/property,這個寫法產生新的member injector

```
public final class Client_MembersInjector implements MembersInjector<Client> {
        private final Provider<Service2> targetProvider;

        public Client_MembersInjector(Provider<Service2> targetProvider) {
                this.targetProvider = targetProvider;
        }

        public static MembersInjector<Client> create(Provider<Service2> targetProvider) {
                return new Client_MembersInjector(targetProvider);
        }

        public void injectMembers(Client instance) {
                injectInjectService(instance, (Service2)this.targetProvider.get());
        }

        public static void injectInjectService(Client instance, Service2 target) {
                instance.injectService(target);
        }
}
```

其實和前面field/property的版本很像,差別在於,它的injectMember使用的method,變成就是委派給你指定@Inject的那個method了

另外我也節錄DaggerInjector的inject method部分

```
        public Client inject(Client client) {
                return this.injectClient(client);
        }

	    private Service2 getService2() {
                return new Service2(new Service());
        }
		private Client injectClient(Client instance) {
                Client_MembersInjector.injectInjectService(instance, this.getService2());
                return instance;
        }
```

他也是一樣委派給Client_MemberInjector,這邊可以看到其實它也是不管,給client產生的injector處理.

所以當你呼叫inject,那如果client裡有指定field/property的話,他就會直接assign進去,但是如果client有指定的method,他就會執行那個method,所以這個method通常就是要在做塞service的事情.

問題就在於文件沒有講,而如果你對DI又不太了解,你可能真的不知道他會幹嘛,而且他其實參數可有可無,所以你如果不知道它是要拿來塞service的,你怎麼會知道怎麼設計這個method,甚至它可以帶參數都不知道,這完全沒有被提到.真的是很誇張.

這個method可以帶參數,它的參數是為你要inject的service type

然而有趣的是,參數你可以塞好幾個,只要它是屬於graph裡面已經有的service,比方我們改寫一下client

```
	@Inject
    fun injectService(target: Service2, target2: Service2) {
        service = target
    }
```

我們多傳一個試試看

```
public final class Client_MembersInjector implements MembersInjector<Client> {
        private final Provider<Service2> targetProvider;
        private final Provider<Service2> target2Provider;

        public Client_MembersInjector(Provider<Service2> targetProvider, Provider<Service2> target2Provider) {
                this.targetProvider = targetProvider;
                this.target2Provider = target2Provider;
        }

        public static MembersInjector<Client> create(Provider<Service2> targetProvider, Provider<Service2> target2Provider) {
                return new Client_MembersInjector(targetProvider, target2Provider);
        }

        public void injectMembers(Client instance) {
                injectInjectService(instance, (Service2)this.targetProvider.get(), (Service2)this.target2Provider.get());
        }

        public static void injectInjectService(Client instance, Service2 target, Service2 target2) {
                instance.injectService(target, target2);
        }
}
```

可以看到client產生的code變成兩個provider,兩個service也被傳入,最後看看injector部分

```
        private Client injectClient(Client instance) {
                Client_MembersInjector.injectInjectService(instance, this.getService2(), this.getService2());
                return instance;
        }
```

直接塞兩個service2進去

所以其實他很智慧,你可以多塞.

而這個方法其實可以解決field/property非public的問題.

#### 小結論

1. @Inject加在constructor,這個class就會被加入一個graph,graph會處理依賴性的問題,當你需要inject這種物件就會幫你create
2. @Inject加在field/constructor,就代表這個field/constructor需要被inject
3. @Inject加在method,代表這個method是一個setter之類的method,method參數type可為在graph裡面的type,當你呼叫injector(component)的inject method,這個method就會被啟動幫你inject

### @Module,@Provides

接下來介紹新的東西,叫做module,還有provider,我們在前面看code時候,client產生的injector裡面可以看到provider的身影,但是實際上component在拿來用的時候都沒有用到provider,所以我們現在來介紹一下怎麼建立provider,有了provider它就能提供更多特別的東西,code在dagger2/Sample3

我們還是先用最簡單的例子,先上個kotlin

```
    @Provides
    @JvmStatic
    fun provideService(): Service = Service()
```

java版本

```
	@Provides static Service provideService() {
    	return new Service();
    }
```

provider本身就是一個function,使用@Provides註解,它必須要是static的,由於kotlin沒static,你可用JvmStatic達到目的.return值當然是service,他是一個提供service的provider,慣例上,provder的命名會用provide當前綴,所以這邊命名為provideService

而provider本身可以有dependency,所以你可以帶入參數,跟前面的constructor injection很像.

```
    @Provides    
    @JvmStatic    
    fun provideService2(dependency: Service): Service2 = Service2(dependency)
```

而provider function必須存在於module裡面,畢竟他是dagger2是java的東西function可不能在class之外

```
@Module
object BaseModule {

    @Provides
    @JvmStatic
    fun provideService(): Service = Service()

    @Provides
    @JvmStatic
    fun provideService2(dependency: Service): Service2 = Service2(dependency)
}
```

java版

```
@Module
class BaseModule {

    @Provides static Service provideService() {
    	return new Service();
    }

    @Provides static Service2 provideService2(Service dependency) {
    	return new Service2(dependency);
    }
}
```

module使用@Module當註解在class上,kotlin的話,因為要用JvmStatic,他只能用在Object不能用在class,所以要用Object,所有的provider都必須在module下面,通常module的命名是XXXModule之類的,習慣性命名法沒有強制力.

當我們有了Module之後,接下來就是要把它和現有的東西結合了,重點在於injector(component)那邊

```
@Component(modules = [BaseModule::class])
interface Injector {
    fun inject(client: Client): Client
}
```

java版本

```
@Component(modules = BaseModule.class)
interface Injector {
    fun inject(client: Client): Client
}
```

在@Component的註解有屬性,加入modules,它對應的是一個array,kotlin的話可以使用arrayOf,或是用\[\]中括號,array type為class

你這樣就可以把數個module都放進去一個component裡面去了.那我們現在當然就要關心一下它變成甚麼樣子了

首先provider每個都拆出一個class出來,我們看其中一個Service的provider好了

```
public final class BaseModule_ProvideServiceFactory implements Factory<Service> {
        public BaseModule_ProvideServiceFactory() {
        }

        public Service get() {
                return provideService();
        }

        public static BaseModule_ProvideServiceFactory create() {
                return BaseModule_ProvideServiceFactory.InstanceHolder.INSTANCE;
        }

        public static Service provideService() {
                return (Service)Preconditions.checkNotNull(BaseModule.provideService(), "Cannot return null from a non-@Nullable @Provides method");
        }

        private static final class InstanceHolder {
                private static final BaseModule_ProvideServiceFactory INSTANCE = new BaseModule_ProvideServiceFactory();

                private InstanceHolder() {
                }
        }
}
```

這個factory的內容幾乎都是static的,它一開始就會自己產生一個instance,non-static則是就是我們真的provider method提供Service,它呼叫的就是BaseModule裡面的provideService,其實就是你剛剛自己寫的static provider function

我們看一下DaggerInjector

```
private Service2 getService2() {
                return BaseModule_ProvideService2Factory.provideService2(BaseModule_ProvideServiceFactory.provideService());
        }
```

之前獲得Service和Service2的方法完全被provider版本的取代了.

#### why

我覺得你可以能會覺得,為什麼要用provider,前面我們@Inject加constructor不是用好好的嗎?

沒錯,實際上provider它和@Inject在constructor的功能是重疊的,所以你現在其實可以把constructor的@Inject拿掉了.你完全不會受影響.這兩種方法只要一個就好了,那既然只要一個provider的存在一定就是為了補強@Inject無法辦到的事

比方說,@Inject的對象不能是interface,@Provides產生的宣告裡面可以是interface對象,當然你最後產生的必須要是實體.這點我們後面還會談到更多,要是可以用interface但是只能出同一種instance的確感覺是沒啥用

如果你有Third-party的class,你不能自己幫她class加@Inject,這時候provider可以直接幫你解決,我們不需要constructor

另外provider後面還會搭配其他東西讓他有更多變化,雖然代表更複雜

#### deprecated 

其實本來provider的function並沒有一定要static,如果你非static,這樣你就會需要Module的實體了,這時候component build的時候會需要額外輸入module的實體,但是這個方法已經被標為過期了,要多設這東西可能會導致遺漏,而且就是寫更多的code,全部static感覺也不會怎樣,所以我也不想要自找麻煩故意將provider的method用非static的方式.沒那麼好學,跳過!

#### 小結論

1. 我們可以使用provider來替代@Inject constructor
2. provider為標記@Provides的method,return你要產生的service,它參數可以是其他的dependency,不過還是需要都在graph內,另外他必須要是static的
3. provider必須存在於module裡面,使用@Module加註class就是module class,然後你在@Component的屬性設置,屬性為modules,它的值是一個module class的array

### 學到這裡可以試著嘗試的東西...

試著開始使用@Inject,@Module,@Component來取代一些new,嘗試一下怎麼去讓dagger2幫我們產生這些object

### @Singleton

我們使用dagger每次幫你製作service,都會是全新的實體,這我們前面看code已經知道很清楚了,但是有時候我們不希望這樣,我希望提供的是同一份reference,這有辦法嗎?當然有.

我們本來有兩種方法指定service,分別是藉由constructor指定@Inject還有provider提供,如果是前者,你在class前面加上@Singleton即可,後者,@Provides註解之外,你再加上@Singleton就可以了

....才怪.......

當你執行的時候,你就會發現報錯了

問題在在於componet,你的module,provider有scope,則component就必須是要有scope的,你provider是singleton,你的module就會是singleton,而你的component就要是singleton,所以你在component那邊也需要加上@Singlton,才會沒問題.(module就邊緣人不用理他),這主要是lifecycle的問題,其實我們module這些有的沒有,它內部的life其實是看它所屬的component決定的,你如果看範例一路走來,你就會發現我們前面唯一一個有被instance的就是DaggerInjector,也就是component,所以這些所有東西的life其實都只是看component的臉色而已,component的lifecycle必須要能足以涵蓋它底下的東西,Component本身可能有很多scope加在上面,但是這些不同的scope的最小公倍數就是component的life長度,我們等一下來看一下scope,而singleton是一種特別的scope,但是你可以自己製作scope,要注意的是雖然你以為@Inject好像沒有用到module,但是其實你東西還是會進graph,而graph就是component.

#### before sample

##### component methods

不過在看一下測試code之前我們要教一些新東西以利我們來觀察,接下來的code,我們對component有一點修改.我們之前在injector的interface裡面寫下了inject method,不過實際上component還能有第二種method

前面我們用的叫做member injection methods,它的用途顧名思義就是拿來inject用的,我們前面提到它的格式,有一個參數,參數為client,可以return void或是client本身, 回傳就是已經注入完畢的了,雖然我們這邊只寫了一個,但是其實你可以有好幾個inject的method

另外一種叫provision methods.這個method的格式是沒有參數,但是他的回傳是你在graph裡面的type,或是Provider\<type\>,Lazy\<type\>,後兩者我們後面再說,總之他就是一個會提供service的function,之後你可以拿它一直call他就會不斷提供新的service,由於我們這邊我們想要來測試singleton,那我們希望他怎麼取都會是同一個reference,所以我們如果有這個method,感覺就是很方便.不需要有inject對象我們就可以不斷取.

#### singleton smaple

這code在dagger2/Sample4

我們現在用兩個service,我們就先不保留相依性,而service1是singleton,service2則否,我們不斷的取出service看看他們的狀況怎樣

```
//Service1
@Singleton
class Service1 @Inject constructor() {
    init {
        println("service1 start time: ${LocalDateTime.now()}")
    }
	fun show() { println("service1 show!") }
}

//service2
class Service2 {
    init {
        println("service2 start time: ${LocalDateTime.now()}")
    }
    fun show() { println("service2 show!") }
}

//injector
@Singleton
@Component(modules = [BaseModule::class])
interface Injector {
    fun getService1() : Service1
    fun getService2() : Service2
}

//main
fun main(args: Array<String>) {

    var injector = DaggerInjector.create()

    repeat(5) { injector.getService1().show() }

    repeat(5) { injector.getService2().show() }

}
```

我們看一下跑的結果

```
service1 start time: 2020-06-14T18:34:29.069
service1 show!
service1 show!
service1 show!
service1 show!
service1 show!
service2 start time: 2020-06-14T18:34:29.072
service2 show!
service2 start time: 2020-06-14T18:34:29.072
service2 show!
service2 start time: 2020-06-14T18:34:29.072
service2 show!
service2 start time: 2020-06-14T18:34:29.072
service2 show!
service2 start time: 2020-06-14T18:34:29.072
service2 show!

Process finished with exit code 0

```

我時間顯示放在constructor,可以看到service1只有出現一次constructor,因為他是singleton,它並沒有創造第二次,所以你不會看到第二次訊息,service2則是每次都會跑constructor,不過他跑太快時間都沒跳,調到100才比較看的出來時間流逝.

我們看一下component的code,它可以說是這次的重點

```
        private void initialize() {
                this.service1Provider = DoubleCheck.provider(Service1_Factory.create());
        }

        public Service1 getService1() {
                return (Service1)this.service1Provider.get();
        }

        public Service2 getService2() {
                return BaseModule_ProvideService2Factory.provideService2();
        }

```

首先兩個get,可以看到它兩者的不同,一個直接由provider提供,一個是用static方法來產生

另外一個就是singleton的重心,我這邊節錄的原因在於,你如果仔細看會發現其實code在其他地方幾乎沒啥變化,那為什麼可以做singleton?

原因其實在這邊他用了這個,這邊多出來的initialize,他有使用這個DoubleCheck來產生provider,如果你追下去看它的功能,你就會發現,其實他本身就是會做reference cache

```
  @Override
  public T get() {
    Object result = instance;
    if (result == UNINITIALIZED) {
      synchronized (this) {
        result = instance;
        if (result == UNINITIALIZED) {
          result = provider.get();
          instance = reentrantCheck(instance, result);
          /* Null out the reference to the provider. We are never going to need it again, so we
           * can make it eligible for GC. */
          provider = null;
        }
      }
    }
    return (T) result;
  }
```

節錄它的get,你可以看到如果他是uninitialized,他會用provider產生新的,但是如果有值就直接return,所以他能做到singleton,而且是很隱諱,其他code都沒有變化.

而這個cache看起來其實就是存在component裡面,所以如果component壽終正寢,那這個cache當然一命嗚呼,因此可以說一切在component下面的,你要馬就是沒scope自生自滅,要馬就是跟component同進退.

#### 小結論

1. @Singleton可以讓service變成singleton而不會每次要求都是新的instance
2. 可以在constructor上面加註,也可以在provider那邊加註,但是重點是你其中一個加註,component就需要加註,因為component才是真正決定lifecycle的地方
3. component的function有前面用的member-injection另外還有一種是provision method,它的作用在於回傳service,或是service的provider,格式為沒有參數直接回傳service type或是Provider\<T\>,如果你只有用provider制定service沒用@Inject,那只能要求Provider回來再用get()取得service

### 學到這裡可以試著嘗試的東西...

singleton一直是很常用的東西,例如很多manager,factory之類的,以往都是用static(kotlin就companion object)的方式處理,現在試著用dagger2來處理吧!singleton直接從constructor傳入後,以後也不用類似getInstance()的方式存取了.

### @Qualifier,@Named

其實Named是Qualifier的一種,所以我們一起講,假設我們有兩個service是同一個type,我們需要它有不同的provider,但是我們在製作provider的時候其實是認type不認其它的,所以會變成你不能擁有多個provider提供同一種type的service

```
//this will not work...
@Module
object BaseModule {

    @Provides
    @JvmStatic
    fun provideService2(): Service2 = Service2()

    @Provides
    @JvmStatic
    fun provideService3(): Service2 = Service2()
}
```

雖然我method名稱是不一樣,但是他可是不會認這個,畢竟你如果有一個Service2要inject,請問你要用第一個還是第二個?

所以這時候就可以有Qualifier登場了.

Qualifier基本上他就像是一個自製的註解,一個tag一樣,在這邊他的目的有點就像是他要給他一個id,所以前面的提供service2的方法會有方法一或是方法二,我們先看看要怎麼製造一個Qualifier

java

```
@Qualifier
@Retention(Runtime)
@Documented
public @interface TestQualifier{
	Color color() default Color.TAN;
	public enum Color{RED,BLACK,TAN}
}
```

其實java在製作自己的註解也是這樣做的

一般來講我們會有上面幾種註解,@Qualifier代表他是一個qualifier,@Rentention代表他的life長度,一般而言是CLASS長度,但是我們需要它在runtime還存活著,所以我們要改成runtime,@Documented就是生成文件會需要被記錄下,這非必要,不過通常自製註解的確會需要被記錄.這樣我們就創造一個qualifer的註解了,另外,在內部定義的就是註解的attributes,就像前面我們@Component(modules=...),modules就是attributes,通常qualifier都會需要一個屬性,因為每個qualifier就是像是一個id,你總要存下這個id,上面的例子是用enum來區分種類.實際上使用你可以有各種形式的資料來當id

kotlin版本寫法

```
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class TestQualifier(val color: Color = Color.TAN){
    enum class Color{RED,BLACK,TAN}
}
```

要使用annotaion class,然後他沒有@Documented,只有@MustBeDocumented

我們前面提到named是qualifier的一種,他是dagger2本來就有提供的,看一下named的定義

```
@Qualifier
@Retention(Runtime)
@Documented
public @interface Named{
	String value() default ""
}
```

所以他就是string來當id

看一下使用例,我們直接用named

```
//target
@Inject
@Named("type1")
lateinit var test1: Type

@Inject
@Named("type2")
lateinit var test2: Type


//provider
@Provides
@Named("type1")
@JvmStatic
fun provideType1() = Type(1)


@Provides
@Named("type2")
@JvmStatic
fun provideType1() = Type(2)

```

雖然type相同但是可以用不同的instance回去,所以這時候provider裡面的interface type就能夠有不同的結果出去了,我們再介紹一種特別的東西,之後我們再來一個範例

### @Binds

code在dagger2/Sample7

如果我們要把一個interface field/property給inject,我們要提供provider

```
    @Provides
    @JvmStatic
    fun provideService(): ServiceInterface = Service1()
```

在這裡的設定我們把ServiceInterface給予一個Service1的實體,而我們現在有一個類似的作法,達到同樣的目的,但是是由dagger2幫你做,要知道這些東西到時候還會生成一些code,相較於上面的寫法,另一種寫法能產生更少的code,並且達到相同的目的

```
@Binds
abstract fun provideService(serviceImp: Service1) : ServiceInterface
```

這種寫法會把ServiceInterface的注入對象都換成Service1的實體,當然前提是Service1本身在graph裡面.

不過這也帶來一些影響,首先我們之前一直用class(kotlin用object)來處理module,問題是現在有了abstract的member,你必須換成abstract class,這連帶的影響provider,有些作法就是將普通provider和binds分成不同的module,然後你在component時候再加起來

然而還有其他寫法,就是利用companion object,companion object就是一個和class同名的object,所以把provider寫進去companion object,呼叫起來像是static呼叫,其實只是因為那個object就跟class同名而已,不過companion object的部分也需要加上@Module

```
@Module
abstract class BaseModule {
	@Module
	companion object {
	 	@Provides
         @JvmStatic
         fun provideService(): Service1 = Service1()
	}
	
	abstract fun provideServiceImp(serviceImp: Service1): ServiceInterface
}
```

總之這是一個弔詭的交換,根據一些說法,使用Binds其實會讓產生的code比較少,但是你要key的code比較多,哈哈哈

### use qualifier and binds for interface

我想利用這兩個東西來做到interface inject的目的,經過了一些小實驗看樣子是可以的,code在dagger2/Sample10

首先先來幾個service

```
//service interface
interface ServiceInterface {
    fun show()
}

//service1
class Service1 : ServiceInterface{
    override fun show() { println("service1") }
}

//service2
class Service2 : ServiceInterface{
    override fun show() { println("service2") }
}
```

client準備注入

```
class Client {

    @Inject
    lateinit var service1: ServiceInterface

    @Inject
    lateinit var service2: ServiceInterface

    fun show() { service1.show(); service2.show() }
}
```

我們這時候需要qualifier,當然你也可以用其他的例如Named,但是我想更直覺一點,讓qualifier的attribute直接是class來分別

```
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ClassQualifier(val classType: KClass<*>){
}
```

kotlin裡面class用KClass\<\*\>,java是 Class\<?\>

然後我們看一下module

```
@Module
abstract class BaseModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideService1(): Service1 = Service1()

        @Provides
        @JvmStatic
        fun provideService2(): Service2 = Service2()

    }

    @ClassQualifier(Service1::class)
    @Binds
    abstract fun provideService1Imp( serviceImp: Service1) : ServiceInterface

    @ClassQualifier(Service2::class)
    @Binds
    abstract fun provideService2Imp( serviceImp: Service2) : ServiceInterface

}
```

有兩個provider提供service1,service2,下面binds根據不同qualifier把interface綁到不同的type

然後我們要記得被inject的部分也要修改,所以改一下client

```
class Client {

    @ClassQualifier(Service1::class)
    @Inject
    lateinit var service1: ServiceInterface

    @ClassQualifier(Service2::class)
    @Inject
    lateinit var service2: ServiceInterface

    fun show() { service1.show(); service2.show() }
}
```

client加上qualifier指定這樣就可以go了

```
fun main(args: Array<String>) {

    val client = Client()

    DaggerInjector.create().inject(client).show()

}
```

```
service1
service2

Process finished with exit code 0
```

另外額外寫的,你在component如果要提供provision method對應interface,也是一樣記得加上qualifier註解就可以了

```
@ClassQualifier(Service1::class)
fun getService1() : ServiceInterface
```

### 學到這裡可以試著嘗試的東西...

我已經幫你嘗試了,就是interface放進dagger2裡面,試試看用@Binds,還有如果有多型的需求,可以用用看qualifier

### bind instance

在我們寫provider的時候,裡面所有的參數都是要對應graph,但是假設我們剛好有些data想要傳進去,問題在於我們也沒有辦法把它們加入graph,有種方法就是使用@BindInstance

code在dagger2/Sample8

```
class Service1(private val data: String ) : ServiceInterface{
    init {
        println("service1 start time: ${LocalDateTime.now()}")
    }

    override fun show() { println("service1 show $data!") }
}
```

我們改寫了Service1,它的constructor現在有一個data string,provider就會變成你需要輸入額外的String了

```
@Module
abstract class BaseModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideService(data: String): Service1 = Service1(data)
    }

    @Binds
    abstract fun provideServiceImp( serviceImp: Service1) : ServiceInterface

}
```

然而如果這樣build一定報錯,因為String沒有在graph內,我們把component那邊改一下

```
@Component(modules = [BaseModule::class])
interface Injector {

    fun inject(client: Client): Client

    @Component.Builder
    interface Builder{
        fun build(): Injector

        @BindsInstance
        fun addInfo(data: String) : Builder

    }

}
```

新增builder設定並且加入bind instance,等於把這個instance放進去graph內,這樣所有需要string都會變成它,其實等於手動加入object到graph,這個method並沒有命名規定,完全只看@BindInstance註解,DaggerInjector也需要小改

```
fun main(args: Array<String>) {

    val client = Client()

    DaggerInjector.builder().addInfo("new added").build().inject(client).show()

}
```

不能使用create(),要先呼叫builder(),把東西送進去graph然後才build(),可以看到我們把字串送進去了

### 學到這裡可以試著嘗試的東西...

有時候有些東西不是你提供的,當是你需要他的,例如很多framwork提供的東西,像android的context,如果你能將他放進去graph裡面不是很棒嗎?你可以試著放放看了,用@BindInstance

### Lazy

dagger2還提供lazy injection,用法就是把Service type換成 Lazy\<Service type\>,基本上會回傳Service Type的地方,你都可以換成Lazy\<T\>

```
class sample {
	@Inject
	lateinit var service : Lazy<Service>
}
```

例如像上面這樣,provision method或是provider也可以回傳Lazy\<T\>,等你呼叫get(),才會inject,如果T是singleton,那所有呼叫的Lazy\<T\>都會是同一個instance,除此以外都會傳回不同的Lazy\<T\>,不過無論如何,對同一個Lazy\<T\>,你不斷呼叫get,它拿到的還是同一個T,所以用起來還是跟沒Lazy一樣的

### Provider

類似Lazy,我們只要有制定service,你除了可以直接拿service instance,還可以拿Lazy版本的,還有一個就是Provider版本的,你可以直接拿它的provider,其實我們的provision方式也只是在裡面委派給provider的get而已,所以拿到provider這件事其實沒有太大的問題.

```
class sample {
	@Inject
	lateinit var provider : Provider<Service>
}
```

### multi-binding

multi-binding也是怪東西,它有分Set和Map,我們先來看一下Set

#### Set

code在dagger2/Sample9

我們在provider裡面加入@IntoSet,就會讓他的結果進入一個Set裡面,這個provider就會變成Set的一個供應來源

```
    @Provides
    @IntoSet
    @JvmStatic
    fun provideString1(): String = "ABC"

    @Provides
    @IntoSet
    @JvmStatic
    fun provideString2(): String = "DEF"

```

平常要是我們使用兩個相同type的provider,他就會報錯,但是這邊不一樣,這邊兩個provider都是set的來源,所以他們其實是一體的.我們在component可以用provision method來取取看,但是我們要取的不是string,是Set\<String\>,同樣的你還可以取Lazy\<Set\<String\>\>或是Provider\<Set\<String\>\>

```
@Component(modules = [BaseModule::class])
interface Injector {

    fun getStringSet(): Set<String>

}
```

```
fun main(args: Array<String>) {

    val injector = DaggerInjector.create()
    println( "${injector.getStringSet()}" )

}
```

把他印出來,結果會是

```
[ABC, DEF]

Process finished with exit code 0
```

另外,不同module在同個component,也是可以放進同個Set

這邊你要注意的是,它產生的是一個Set\<T\>的東西,所以跟T type是不一樣的,比方上面的例子你如果要取String,你會找不到.

另外,你可以放一個Set進來,就會合併,使用@ElementsIntoSet來合併Set

```
    @Provides
    @ElementsIntoSet
    @JvmStatic
    fun provideStrings1(): Set<String> = setOf("GHI","JKL")
```

```
[ABC, DEF, GHI, JKL]

Process finished with exit code 0
```

另外,它可以搭配前面有提過的qualifier/named來運用,本來一個component只有一個set,設定之後,你就可以有很多不同的Set

```
//module
@Module
object BaseModule {

    @Provides
    @IntoSet
    @JvmStatic
    fun provideString1(): String = "ABC"

    @Provides
    @IntoSet
    @JvmStatic
    fun provideString2(): String = "DEF"

    @Provides
    @ElementsIntoSet
    @JvmStatic
    fun provideStrings1(): Set<String> = setOf("GHI","JKL")

	//this provider with named
    @Provides
    @IntoSet
    @Named("number")
    @JvmStatic
    fun provideString3(): String = "123"

}
```

```
@Component(modules = [BaseModule::class])
interface Injector {

    fun getStringSet(): Set<String>

    @Named("number")
    fun getNumberStringSet(): Set<String>

}
```

```
fun main(args: Array<String>) {

    val injector = DaggerInjector.create()
    println( "set1:${injector.getStringSet()},set2:${injector.getNumberStringSet()}" )

}
```

```
set1:[ABC, DEF, GHI, JKL],set2:[123]

Process finished with exit code 0
```

#### map

接下來我們看一下map,相對於Set,Map就是用@IntoMap就可以把他加入到一個Map,由於Map是可以對應的,的確感覺可以處理多型的問題,但是我們都知道Map會需要的是key,所以單純只是value還不夠,你還需要提供key

目前dagger2提供幾種key可以使用,ClassKey,IntKey,LongKey,StringKey,你也可以客製

它其實和前面Set差不多,你設定註解,然後就放在Map裡面,你拿出來的話是Map,差異在於你每個provider還要多提供key

```
    @Provides
    @IntKey(1)
    @IntoMap
    @JvmStatic
    fun provideString1(): String = "ABC"

    @Provides
    @IntKey(2)
    @IntoMap
    @JvmStatic
    fun provideString2(): String = "DEF"
```

```
@Component(modules = [BaseModule::class])
interface Injector {

    fun getStringMap(): Map<Int,String>
    
}
```

```
fun main(args: Array<String>) {

    val injector = DaggerInjector.create()
    println( "value1:${injector.getStringMap().get(1)},value2:${injector.getStringMap().get(2)}" )

}
```

```
value1:ABC,value2:DEF

Process finished with exit code 0
```

如果你key相同的狀況,它會報錯.取值的時候,如果key沒有對應會回傳null,這在kotlin上可能要注意.

它也是可以加qualifier,形成不同的map

我們來看一下自製key,雖然它default提供就夠多了,但是總有更怪的需求,你甚至可以複合


```
enum MyEnum {
  ABC, DEF;
}

@MapKey
@interface MyEnumKey {
  MyEnum value();
}

@MapKey
@interface MyNumberClassKey {
  Class<? extends Number> value();
}

@MapKey(unwrapValue = false)
@interface MyKey {
  String name();
  Class<?> implementingClass();
  int[] thresholds();
}
```

上面這是我從文件照抄的,不過是java版本,我想把它轉換成kotlin版,也花了不少時間


```
enum class MyEnum{
    ABC,DEF
}

@MapKey
annotation class MyEnumKey(val value: MyEnum){
}

@MapKey
annotation class MyEnumClassKey(val value: KClass<out Number>){
}

@MapKey(unwrapValue=false)
annotation class ComplexKey(val name:String,val implementClass: KClass<*>,val thresholds: IntArray){}

```

上面會看到一些怪東西,主要還是在複合key那邊,我們可以看到突然跑一個unwrapValue,這個後來查文件才知道,因為MapKey是需要單一物件比對,複合key有好幾個,如果你設定為true,它就會拿第一個屬性當key,後面的廢掉,當然我們希望這些東西都保留且有影響力,所以你要設定false

而接下來就是剛剛的問題,你需要把你的註解的屬性都能打成一包,這樣你在runtime的時候它才能幫它當key,map才能get(key)

教學裡面是介紹一個辦法,說是利用@AutoAnnotation來處理,然後丟一行碼就沒了,只能說不意外.

目前看起來應該是@AutoAnnotation將註解的屬性組合成一個object來當key,不過問題是你還需要研究一下auto value這個lib,我決定不理他 

然而,map也可以放在@Binds上面

```
@Binds
@IntoMap
@ClassKey(ClassImpA::class)
abstract fun bindImplement(imp: ClassImpA): ClassInterface
```

Map其實和前面提到的qualifier,能做到類似的東西,和qualifier相比,它其實能弄出一個像factory的機制傳回一個Map,讓你去選擇要的東西,和前面qualifier的範例比起來,Map需要寫的東西可能比較少吧?(不過產出的code就不一定了),但是qualifer的做法比較像是還是將factory的功能給dagger負責,而Map則是自己取得這個Map,等於factory還是要你自己來implement,但是有時候就是有這種狀況.比方說你的fatcory其實不是給你自己用而是給系統或是別人用的,這時候Map的方法就可以用.

#### @Multibinds

當你有了IntoSet,或是IntoMap之類的,你會產生Set或是Map,但是假如,你想要這個Set或是Map,但是你又還沒加入IntoSet或是IntoMap,這樣的話它就不會產生Set/Map,你寫對應的code,它就會說你graph沒有,報錯.這時候@Multibinds就登場了,它可以事先告訴graph,我以後會有Set/Map,這樣你製造provision等就不會有問題

```
@Module
abstract class MyModule {
  @Multibinds abstract aSet(): Set<Foo>
}
```

它寫法和@Binds很像.這樣就事先告訴graph我們會有一個Set的provider,而實際上這東西會等到IntoSet之類的才真的有set,不會有甚麼影響.

### 學到這裡可以試著嘗試的東西...

和前面使用qualifier和@Binds來將dagger2變成interface的factory不同,這次試著拿取map,自己做factory吧,直接獲得一個interface的provider,讓你根據不同的class key來取得不同的implement

### scope

scope完全沒有多講這到底怎麼運作.稍微記錄一下.其實singleton也算一種scope,不過他是dagger2提供的.所以他在應用上其實就跟singleton一樣.

首先上[文件](https://docs.oracle.com/javaee/7/api/javax/inject/Scope.html)

基本上他就像是一個自製的註解,一個tag一樣,當你將這個標在service的@Inject class上,或是provider,就等於制定它的scope,你沒有制定它就每次都產生新的.先看一下scope你要怎麼定義

java

```
@Scope
@Retention(RUNTIME)
@Documented
public @interface MyScope{
}
```

這樣就是一個新的scope.我們前面講qualifier的時候也用過,基本上scope也是一種自製註解.

kotlin的寫法

```
@Scope
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotaion class MyScope{
}
```

前面提到scope會作用在service,provider,component,其實是component決定的,我們將註解或是tag標註在某個service上面,或是provider上面,然後就會迫使收容它的component必須要有跟他相同或是更大的scope指定,而到最後所有component底下有scope的東西都會跟component同生共死,因為他們的cache都存在component上面.聽起來有點複雜

其實scope只是一個標記,他的功用就是這些跟我同樣標記的,就是同生共死的好朋友(壽命一樣的),所以service或是provider如果有標記他是A壽命的,那他的component一定要有標記壽命,不能是無標記的,但是這邊就是有趣的點了,component不能沒有壽命標記,但是他可以是任何壽命標記,比方說壽命B,因為壽命A和壽命B代表的只是它們的命不一樣長,但是可不代表誰的命比較長,唯一一個知道壽命長度的標記就是@Singleton,他就是你最長的壽命可能性,但是其他標記的壽命只是代表區別而已,不代表長短.

而實際上決定壽命的長短,還是在於component,因為component是會被實體化的,所以他何時被release就是壽終了.

我們之前的思考是,我希望這個service的壽命多長,所以我們去把他標記scope,然後希望同樣長命的service標同樣的scope.而最後影響到component.

但是實際的思維應該是,我希望有一個壽命長度,然而我就要先有一個component的壽命長度是這樣,我們給他一個scope,然後我把想要這麼長命的service都聚集到這個component的門下.也幫他標記同一個scope.接著就去操作component instance的生命,讓他在你想要的週期內生或死.

所以如果我們想要有不同life的scope,就要有不同的component,我們就勢必要有多個component一起合作了,所以接下來我們要探討的就是多個component之間的交互和組合.

### Component,dependencies,sub-componet

我們前面講過component,它製造injector,結合module,提供inject方法或是provision方法.每個component基本上都有它的scope,就是lifecyle,一般如果沒有指定scope,其實不需要太在意它,裡面東西就是每次都是new instance,但是如果你有scope,它的lifecycle就很重要,但是也會變得很麻煩.我們先不要看scope,就先只要專注在component的組合就好了,這些component要怎麼合作?通常有兩種方法,一種叫dependency,一種是sub-component

#### dependencies

code在dagger2/Sample5

@Component的attributes我們前面有看過modules,另外一個叫dependencies,它也是一個array,裡面item是class,不過這邊是component的class,dependency代表component之間的依賴性,我們可以藉由這種方法讓它們合併.

我們這次範例有兩個service,和前面一個一樣,這次有兩個component,我們也先多弄一個client備用,client2關聯service1,純粹只是拿來驗證用

```
//injector1
@Component(modules = [BaseModule::class],dependencies = [Injector2::class])
interface Injector {
     fun inject(client: Client): Client
     
     fun inject(client: Client2): Client2
}

//injector2
@Component
interface Injector2 {
	fun inject(client: Client2): Client2
}

//client2
class Client2 {
    @Inject
    lateinit var service1: Service1

    fun show() { service1.show() }
}
```

可以看到我們藉由dependencies將它合併起來了,然後接下來下一個重點看一下main

```
fun main(args: Array<String>) {

    val client = Client()
    val injector = DaggerInjector.builder().injector2(DaggerInjector2.create()).build()

    injector.inject(client).show()

}
```

我們之前使用DaggerInjector.create()

現在無法使用了,你可以看產生的code,現在沒有create了

```
public final class DaggerInjector implements Injector {
        private DaggerInjector(Injector2 injector2) {
        }

        public static DaggerInjector.Builder builder() {
                return new DaggerInjector.Builder();
        }

        public Client inject(Client client) {
                return this.injectClient(client);
        }
        
        public Client2 inject2(Client2 client) {
                return this.injectClient2(client);
        }


        private Client injectClient(Client instance) {
                Client_MembersInjector.injectService1(instance, new Service1());
                Client_MembersInjector.injectService2(instance, BaseModule_ProvideService2Factory.provideService2());
                return instance;
        }

        public static final class Builder {
                private Injector2 injector2;

                private Builder() {
                }

                /** @deprecated */
                @Deprecated
                public DaggerInjector.Builder baseModule(BaseModule baseModule) {
                        Preconditions.checkNotNull(baseModule);
                        return this;
                }

                public DaggerInjector.Builder injector2(Injector2 injector2) {
                        this.injector2 = (Injector2)Preconditions.checkNotNull(injector2);
                        return this;
                }

                public Injector build() {
                        Preconditions.checkBuilderRequirement(this.injector2, Injector2.class);
                        return new DaggerInjector(this.injector2);
                }
        }
}
```

很簡單因為你現在有dependency,和DaggerInjector2有關係,所以你在build出來前你需要Injector2的實體.

```
DaggerInjector.builder().injector2(DaggerInjector2.create()).build()
```

所以你需要先弄出builder,呼叫新的injector2 method讓他注入injector2,然後才build()

然而你會發現,在DaggerInjector裡面完全沒有injector2的蹤跡,好吧,的確injector2本來就沒啥,只有一個inject,但是他也沒有出現在injector1這邊,所以你也不能用它,根據文件的說法,你這種做法,它其實只會把dependency的provision method給他用,記得是provision method喔,不是graph,你看不到injector2的graph,injector2現在沒有任何provision method,所以甚麼都不會發生.

我們試著來做一些改變,你可以注意到,Service1我們用的是@Inject的方式指定,但是在有兩個component的狀況下,它會跑去哪個injector?如果你有看兩個injector的code,你會發現他兩個都會出現,但是如果是service2,那injector2是找不到的,因為他是藉由module進去的.

我們來稍微修改一下,這次我把injector2產生一個provision method提供Service1

```
@Component
interface Injector2 {
    fun getService1() : Service1 //add
    fun inject(client: Client2): Client2
}

```

然後我們來build,看看DaggerInjector

```
public final class DaggerInjector implements Injector {
        private final Injector2 injector2;

        private DaggerInjector(Injector2 injector2Param) {
                this.injector2 = injector2Param;
        }

        public static DaggerInjector.Builder builder() {
                return new DaggerInjector.Builder();
        }

        public Client inject(Client client) {
                return this.injectClient(client);
        }

        public Client2 inject2(Client2 client) {
                return this.injectClient2(client);
        }

        private Client injectClient(Client instance) {
                Client_MembersInjector.injectService1(instance, (Service1)Preconditions.checkNotNull(this.injector2.getService1(), "Cannot return null from a non-@Nullable component method"));
                Client_MembersInjector.injectService2(instance, BaseModule_ProvideService2Factory.provideService2());
                return instance;
        }

        private Client2 injectClient2(Client2 instance) {
                Client2_MembersInjector.injectService1(instance, (Service1)Preconditions.checkNotNull(this.injector2.getService1(), "Cannot return null from a non-@Nullable component method"));
                return instance;
        }

        public static final class Builder {
                private Injector2 injector2;

                private Builder() {
                }

                /** @deprecated */
                @Deprecated
                public DaggerInjector.Builder baseModule(BaseModule baseModule) {
                        Preconditions.checkNotNull(baseModule);
                        return this;
                }

                public DaggerInjector.Builder injector2(Injector2 injector2) {
                        this.injector2 = (Injector2)Preconditions.checkNotNull(injector2);
                        return this;
                }

                public Injector build() {
                        Preconditions.checkBuilderRequirement(this.injector2, Injector2.class);
                        return new DaggerInjector(this.injector2);
                }
        }
}
```

你可以看到這次跑出很多injector2相關的東西了.Service1的東西變成injector2來提供.injector2的實體甚至還被cache在injector1裡面.所以當你合併後,你可以用2的provision method來產生新的service.

而藉這個機會我們來順便弄scope進來.我們先弄兩個scope,scope1,scope2.

```
//scope1
@Scope
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Scope1 {
}

//scope2
@Scope
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Scope2 {
}
```

我們這邊交叉實驗了幾個狀態,來看一下結果

1. injector1使用scope1,injector2使用scope2,沒問題
2. injector1,injector2使用同一個scope,錯誤
3. injector1沒有scope,injector2有scope,錯誤
4. Injector1有scope,injector2沒有scope,沒問題
5. injector1,injector2使用singleton,錯誤
6. injector1有singleton,injector2有scope,錯誤
7. injector1有singleton,injector2沒有scope,沒問題
8. injector1沒有scope,injector2有singleton,錯誤
9. injector1有scope,injector2有singleton,沒問題

結論

injector1依賴injector2的狀況下,injector1的scope一定要小於injector2的scope,因為如果你injector1還沒用完你inject2就壽終正寢,那不就麻煩大了嗎?而且從builder那邊就可以看的出來,你是先建立injector2的實體,然後注入injector1 builder,然後才建立injector1,所以injector2的壽命一定會比injector1長.

* 沒有scope的不能依賴有scope的,包含singleton
  => 沒scope的東西每次都會有不同的實體,有scope代表可能同一段時間你都會得到同一個實體,不同實體去依賴同一個東西就會有side effect.
* singleton唯一能依賴沒scope的,其他都不行.
  => 沒有人壽命比singleton長了,所以他的依賴沒人能比他長命了,除非用沒scope的
* 有scope能依賴所有對象,但是不能跟自己一樣的
  => 這代表他們的命長度不一樣(只是說不一樣喔,沒說誰長誰短),但是兩個都不是singleton,所以沒問題.

#### sub-component

前面我們看完dependency,主要component可以看到依賴component的所有provision method,而sub-component不一樣,首先它是sub可以看到parent的東西,而且他看到的是graph,不是provision method這種小case而已,然而parent不行看sub的東西,基本上這又是一個沒啥範例文件.而且很複雜很麻煩.

sub掛到parent這邊有兩種方法,我們來看範例,code在dagger2/Sample6

我們這次用了一個component,兩個subcomponent,為了測試一些東西,Service1的@Inject拿掉改成provider,新增一個SubModule來放這個provider

```
@Component
interface Injector {

    val subInjector1: SubInjector1

}

//sub module
@Module(subcomponents = [SubInjector2::class])
object SubModule {

    @Provides
    @JvmStatic
    fun provideService1(): Service1 = Service1()
}

//sub injector1
@Subcomponent(modules = [SubModule::class])
interface SubInjector1 {

    fun getService1() : Service1

    val subInjector2Builder : SubInjector2.Builder

}

//sub injector2
@Subcomponent(modules = [BaseModule::class])
interface SubInjector2 {

    fun getService2(): Service2

    @Subcomponent.Builder
    interface Builder {
        fun build(): SubInjector2
    }

    fun inject(client: Client): Client
}
```

sub1有service1,sub2有service2,sub2還有inject

另外你可以看到它們另一個差異在於sub2多一個叫builder的東西.

先說明一下subComponent掛到parent上面有兩種方法,在這個例子裡面,我將sub1掛在injector下面,然後sub2掛在sub1,分別個用一種方法.

方法一可以說有夠簡單,就是直接在parent component的interface底下多一個method,回傳sub-component回去,照文件的說法,這個method的參數為module,然後回傳是sub-component本身,但是我看一些文章其實完全沒有在管module實體,直接省略參數,後來思考一下,發現原因是因為module裡面的provider都已經寫成static了,所以其實不需要instance,在這種前提下,kotlin還可以大幅進化,因為kotlin都是用property,本身都會自己加上getter/setter,所以如果你用val直接會送一個setter,就可以使用

```
@Component
interface Injector {

    val subInjector1: SubInjector1

}
```

省了很多工.

另外第二種方法,它是把它掛在parent的module裡面,@Module有一個屬性叫subcomponents,你如果在這邊設定對應的subcomponent,那你的sub-component就會需要builder,不然它會叫錯誤,使用這個方法,它只有在真正需要sub的時候才會喚起他,而在parent裡面你需要加入Builder將它顯現,才可以使用subInjector2

```
//sub module
@Module(subcomponents = [SubInjector2::class])
object SubModule {

    @Provides
    @JvmStatic
    fun provideService1(): Service1 = Service1()
}

//sub injector1
@Subcomponent(modules = [SubModule::class])
interface SubInjector1 {

    fun getService1() : Service1

    val subInjector2Builder : SubInjector2.Builder

}
```

sub1不管你會不會用到,他一定會被instance,不過他可以少寫一點不用管builder,sub2則是不用到就沒有instance,因為他只有builder instance

```
fun main(args: Array<String>) {

    val client = Client()

    DaggerInjector.create().subInjector1.subInjector2Builder.build().inject(client).show()

}
```

我們看一下main,這邊我們把sub2給弄起來之後show,你會發現它work,sub1有service1,sub2有service2,但是client需要兩個,結果兩個都會被看見,另外我們看一下生成code,你會發現只有一個DaggerInjector,沒有兩個sub的版本,當你實際上build的時候,sub的內容會被包進去,我們看一下parent這邊

```
public final class DaggerInjector implements Injector {
        private DaggerInjector() {
        }

        public static DaggerInjector.Builder builder() {
                return new DaggerInjector.Builder();
        }

        public static Injector create() {
                return (new DaggerInjector.Builder()).build();
        }

        public SubInjector1 getSubInjector1() {
                return new DaggerInjector.SubInjector1Impl();
        }

        private final class SubInjector1Impl implements SubInjector1 {
                private SubInjector1Impl() {
                }

                public Service1 getService1() {
                        return SubModule_ProvideService1Factory.provideService1();
                }

                public bj.samples.dagger2.components.SubInjector2.Builder getSubInjector2Builder() {
                        return new DaggerInjector.SubInjector1Impl.SubInjector2Builder();
                }

                private final class SubInjector2Impl implements SubInjector2 {
                        private SubInjector2Impl() {
                        }

                        public Service2 getService2() {
                                return BaseModule_ProvideService2Factory.provideService2();
                        }

                        public Client inject(Client client) {
                                return this.injectClient(client);
                        }

                        private Client injectClient(Client instance) {
                                Client_MembersInjector.injectService1(instance, SubModule_ProvideService1Factory.provideService1());
                                Client_MembersInjector.injectService2(instance, BaseModule_ProvideService2Factory.provideService2());
                                return instance;
                        }
                }

                private final class SubInjector2Builder implements bj.samples.dagger2.components.SubInjector2.Builder {
                        private SubInjector2Builder() {
                        }

                        public SubInjector2 build() {
                                return SubInjector1Impl.this.new SubInjector2Impl();
                        }
                }
        }

        public static final class Builder {
                private Builder() {
                }

                public Injector build() {
                        return new DaggerInjector();
                }
        }
}
```

可以看到,兩個sub變成inner class,sub1在parent裡面,sub2在sub1裡面,所以才會有sub可以看到parent,parent看不到外面這種說法.也代表sub其實沒有產生自己的class,它會依附在對應的parent裡面,沒有parent就沒有問題,所以即使我們在sub2裡面放inject,但是sub2的graph本身並不能看到全部service,這也不會像之前component一樣會回報graph找不到的問題.

sub-component會繼承parent的graph,而前面提過的multi-bind的Set,Map也是一樣,sub-component可以加parent的set/map,不過parent的部分還是老樣子你看不到sub加的東西

scope可以說是subCom另一個重要的要素,所有的subCom的scope必須小於他的parent,所以你不能讓com和subcom使用同一個scope,但是不同的subcom,在同一個com底下,他們可以有不同的scope,也可以有同一個scope,而且因為他們算不同component,即使scope name一樣,也都是有獨立的lifecyle.通常我們在root com這邊都會設定Singleton,因為sub com必須小於他.singleton是最大的了.

#### some reference

由於這東西真的很麻煩,可以參考一些文章

* 這篇[文章](http://jellybeanssir.blogspot.com/2015/05/component-dependency-vs-submodules-in.html),比較dependency和sub-component的使用,也有舉例
* 這篇[文章](http://frogermcs.github.io/dependency-injection-with-dagger-2-custom-scopes/),將scope和sub-component結合,也是蠻值得看,另外他有提到怎麼控制scope life這件事,其實就是適當的時機把component給清除掉.
* 這篇[文章](https://proandroiddev.com/dagger-2-part-ii-custom-scopes-component-dependencies-subcomponents-697c1fa1cfc),將android,scope,sub-component的設計,可以看看

### 學到這裡可以嘗試的東西

比起depednecies,sub-component可能是比較常用的選擇,試著做出不同life的sub-component來操作一下吧.

### @Reusable

我們知道service在沒scope下每次都會有新instance,但是如果我們不想那麼浪費,卻又想要維持這種機制,這時候就可以有reusable.reusable會cache住每個binding的結果,或是instance的object.就不會每次叫都產生.如果之前已經有cache的,會使用cache.
比較特別的是,前面提過scope,你需要provider或是constructor,component都同個scope,這個註解基本上不弄在component,只在provider或是constructor,他建議的是你用在本來non-scope的東西上,例如immutable的object(每次都要new).他比較是像是省記憶體用的,不會一直狂new.但是他基本上還是根據service所在的component,也就是一個component只會cache一個.而且這要很小心用,如果你本來就希望他只有一個,那你可能考慮scope會比較好.因為這邊的reuse可能牽扯到side effect,畢竟他是reference,所以一個immutable object就可以使用他,因為你內部不會改變,就不會有side effect,影響到其他reference他的地方.所以使用要很小心.

放最後面因為他其實不是很重要,哈哈哈

### test

 它有幾個test建議

1. 製作測試的module class繼承要被測試的module,override想要測試的provider,它認為這不是很好的方法,因為module的實體,如果你是從builder放進去,基本上他還是會認原來的class,所以你的假module的新provider無法加進去(除非你要去改component的modules list,這蠻麻煩的),另外假provider如果有參數它也需要保留,那就是代表他的相依性會保留,實際上我們知道dagger只認回傳type其實不管其他的,所以你的彈性就變少了
2. 比較好的方式它建議是從component level下手,作假的component,基本上component只會出現在instance的地方其實不多見,另外彈性也比較大
3. module的provider grouping,最好把對外或對內分不同module,會比較好測試
4. 如果你只有一個class要測試,你不需要用dagger,你直接手動把東西inject進去就好了,不需要搞得那麼麻煩.

### android dagger

android dagger差不多跟死沒兩樣,所以就不提了,但是沒想到,他現在好像跑出一個東西叫dagger hilt,很新,超新!新到不行

所以不知道炒不炒得起來,但是真的累死人

### 自己的心得

東西雖然很多,但是感覺其實都有用到的機會

* @Inject:dagger2直接成為一個超大的factory,產生各種object,我想最大的特色在於object之間的依賴,原本假如A依賴B,B依賴C,A的class constructor或是應用B的method,你會需要傳入C當作參數,如果你採用自己產生依賴instance的方法,原本A的依賴是B,卻因為要產生B所以變成A需要C,當你B如果需要更多的依賴,或是移除C的依賴,A都需要改變.如果你採用外部注入的方式,你就會發現A不需要知道C,他只要知道B,不會受到影響,只需要關心直接依賴的東西,而不需要關心依賴的依賴,所以不管你是不是用dagger2來做外部注入,constructor injection都是一種好的寫法,而使用dagger2讓你省去外部產生instance的步驟,而且還幫你注入,會發現省了不少code,而且他自動幫你處理誰要先產生的順序問題,所以如果有人問你,使用dagger2有啥好處,我覺得我還是寫很多code啊?長得很怪又難學,與其跟他講說因為可以DI這種虛無縹緲的答案,通常這樣講的都不太清楚啦~直接用上面的例子講我想他會更快理解使用dagger2的好處,我想如果要說有缺點,就是DI本來的問題,你看code的人可能要跳很多class才知道所有的依賴,不過這不就是我們要的嗎?解除依賴的層層鏈結.
* @Binds:很意外地會發現這東西還不錯,主要是要搭配比方說DIP,在寫的時候我們會把依賴的對象指定為interface,而非class,然而你總有要實作的時候,由於我們的generator都是dagger2負責,使用@Binds可以輕鬆的將class和interface綁在一起,雖然interface很多時候利用在多型,代表他可能面對的是很多種class而非一種,但是類似DIP這種寫法我們其實可能當下真的實作對象只有一個,只是因為你要decouple它們的直接依賴才使用interface,希望在遙遠或是可能的未來我們會重複利用這個class,或是只是想讓你看起來像是一個很屌的程式設計員,才把它的依賴給interface,所以在應用區的部分就直接@Binds起來,你就不需要多寫,然而當然缺點是,code難讀,你會發現明明對的是interface卻找不太到instance的實體化,decouple不是叫假的,拆到連你看code都需要花時間去對一下.才找的到依賴實體.
* @IntoMap:如果你需要factory class,我想沒甚麼比他更好的東西了,產生一個class key,就可以生出一個map\<class,Provider\<Interface\>\>的map,完完全全就是factory,你可以搭配@Binds,將這個interface和不同的class綁在一起,key是class,很完美,當然前提是你需要factory啦,你如果不需要的話其實給dagger2負責就好,但是如果你要自製factory出來,用這樣寫快到不行,啥if或是switch都不用了.
* @BindInstance:主要是如果你有一個非你製造出來的物件要加入component,那他就是你的選擇,現在寫程式很多時候都base在framework上,系統的東西如果你要加入dagger2能提供的依賴行列,那就是他了,有了他framework的東西也可以當依賴,你的component就更完美了
* scope,@Subcomponent:我想,使用sub-component的原因不外乎就是要有不同scope,不然何必自找麻煩,當你面對不同的lifecyle,希望提供的東西有長短的lifecyle,那就是這兩個東西出場的時機.你只要維護sub-component實體的lifecyle,就可以達到你要的效果,但是只能說我希望你不要用到他,因為當你需要這樣做,那代表你的project其實還蠻難複雜的,不過android的這種各種lifecycle的東西,通常就會需要他出馬.

### more resource

android and dagger2: https://www.raywenderlich.com/6583167-dagger-2-tutorial-for-android-advanced#toc-anchor-011

api: https://dagger.dev/api/2.13/overview-summary.html

沒屁用的dagger2官網: https://dagger.dev/






