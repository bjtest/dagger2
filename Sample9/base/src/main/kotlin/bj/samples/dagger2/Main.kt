package bj.samples.dagger2

import bj.samples.dagger2.components.DaggerInjector


fun main(args: Array<String>) {

    val injector = DaggerInjector.create()
    println( "set1:${injector.getStringSet()},set2:${injector.getNumberStringSet()}" )

}