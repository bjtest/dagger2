package bj.samples.dagger2.modules

import dagger.Module
import dagger.Provides
import dagger.multibindings.ElementsIntoSet
import dagger.multibindings.IntoSet
import javax.inject.Named

@Module
object BaseModule {

    @Provides
    @IntoSet
    @JvmStatic
    fun provideString1(): String = "ABC"

    @Provides
    @IntoSet
    @JvmStatic
    fun provideString2(): String = "DEF"

    @Provides
    @ElementsIntoSet
    @JvmStatic
    fun provideStrings1(): Set<String> = setOf("GHI","JKL")

    @Provides
    @IntoSet
    @Named("number")
    @JvmStatic
    fun provideString3(): String = "123"
    
}