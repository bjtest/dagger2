package bj.samples.dagger2

import javax.inject.Inject

class Client {
    @Inject
    lateinit var data: String

    fun show() { println("$data") }
}