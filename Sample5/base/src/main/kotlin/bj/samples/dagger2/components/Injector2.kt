package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.Client2
import bj.samples.dagger2.Service1
import bj.samples.dagger2.scopes.Scope1
import bj.samples.dagger2.scopes.Scope2
import dagger.Component
import javax.inject.Singleton

@Scope2
@Component
interface Injector2 {
    fun getService1() : Service1
    fun inject(client: Client2): Client2
}