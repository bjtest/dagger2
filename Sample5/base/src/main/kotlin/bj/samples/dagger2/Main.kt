package bj.samples.dagger2

import bj.samples.dagger2.components.DaggerInjector
import bj.samples.dagger2.components.DaggerInjector2


fun main(args: Array<String>) {

    val client = Client()
    val injector = DaggerInjector.builder().injector2(DaggerInjector2.create()).build()

    injector.inject(client).show()

}