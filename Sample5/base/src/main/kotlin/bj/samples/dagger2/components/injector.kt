package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.Client2
import bj.samples.dagger2.Service1
import bj.samples.dagger2.Service2
import bj.samples.dagger2.modules.BaseModule
import bj.samples.dagger2.scopes.Scope1
import dagger.Component
import javax.inject.Provider
import javax.inject.Singleton

@Scope1
@Component(modules = [BaseModule::class],dependencies = [Injector2::class])
interface Injector {
    fun inject(client: Client): Client

    fun inject2(client: Client2) : Client2
}