package bj.samples.dagger2

import javax.inject.Inject

class Client2 {
    @Inject
    lateinit var service1: Service1

    fun show() { service1.show() }
}