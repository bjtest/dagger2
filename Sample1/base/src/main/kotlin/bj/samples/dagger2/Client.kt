package bj.samples.dagger2

import javax.inject.Inject

class Client {
    @Inject
    lateinit var service: Service

    fun show() { service.show() }
}