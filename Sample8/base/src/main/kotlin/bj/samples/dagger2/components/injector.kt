package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.modules.BaseModule
import dagger.BindsInstance
import dagger.Component

@Component(modules = [BaseModule::class])
interface Injector {

    fun inject(client: Client): Client

    @Component.Builder
    interface Builder{
        fun build(): Injector

        @BindsInstance
        fun addInfo(data: String) : Builder

    }

}