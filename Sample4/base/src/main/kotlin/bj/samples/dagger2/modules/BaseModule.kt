package bj.samples.dagger2.modules

import bj.samples.dagger2.Service1
import bj.samples.dagger2.Service2
import dagger.Module
import dagger.Provides

@Module
object BaseModule {

    @Provides
    @JvmStatic
    fun provideService2(): Service2 = Service2()
}