package bj.samples.dagger2.components

import bj.samples.dagger2.Service1
import bj.samples.dagger2.Service2
import bj.samples.dagger2.modules.BaseModule
import dagger.Component
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
@Component(modules = [BaseModule::class])
interface Injector {
    fun getService1() : Service1
    fun getService2() : Service2
}