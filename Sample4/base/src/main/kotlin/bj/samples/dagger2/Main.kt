package bj.samples.dagger2

import bj.samples.dagger2.components.DaggerInjector


fun main(args: Array<String>) {

    var injector = DaggerInjector.create()

    repeat(5) { injector.getService1().show() }

    repeat(5) { injector.getService2().show() }

}