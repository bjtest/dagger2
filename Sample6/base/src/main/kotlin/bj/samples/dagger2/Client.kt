package bj.samples.dagger2

import javax.inject.Inject

class Client {
    @Inject
    lateinit var service1: Service1

    @Inject
    lateinit var service2: Service2


    fun show() { service1.show(); service2.show() }
}