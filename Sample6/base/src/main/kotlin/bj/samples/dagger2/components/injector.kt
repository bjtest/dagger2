package bj.samples.dagger2.components

import bj.samples.dagger2.modules.SubModule
import dagger.Component

@Component
interface Injector {

    val subInjector1: SubInjector1

}