package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.Service2
import bj.samples.dagger2.modules.BaseModule
import dagger.Subcomponent

@Subcomponent(modules = [BaseModule::class])
interface SubInjector2 {

    fun getService2(): Service2

    @Subcomponent.Builder
    interface Builder {
        fun build(): SubInjector2
    }

    fun inject(client: Client): Client
}