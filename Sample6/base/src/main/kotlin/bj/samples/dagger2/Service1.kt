package bj.samples.dagger2

import java.time.LocalDateTime
import javax.inject.Inject
import javax.inject.Singleton

class Service1/* @Inject constructor() */{
    init {
        println("service1 start time: ${LocalDateTime.now()}")
    }

    fun show() { println("service1 show!") }
}
