package bj.samples.dagger2.modules

import bj.samples.dagger2.Service1
import bj.samples.dagger2.components.SubInjector2
import dagger.Module
import dagger.Provides

@Module(subcomponents = [SubInjector2::class])
object SubModule {

    @Provides
    @JvmStatic
    fun provideService1(): Service1 = Service1()
}