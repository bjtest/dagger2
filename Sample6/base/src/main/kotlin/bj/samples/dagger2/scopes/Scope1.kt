package bj.samples.dagger2.scopes

import javax.inject.Qualifier
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Scope1 {
}
