package bj.samples.dagger2

import java.time.LocalDateTime

class Service2 {
    init {
        println("service2 start time: ${LocalDateTime.now()}")
    }

    fun show() { println("service2 show!") }
}