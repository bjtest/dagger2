package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.Service1
import bj.samples.dagger2.Service2
import bj.samples.dagger2.modules.BaseModule
import bj.samples.dagger2.modules.SubModule
import dagger.Subcomponent

@Subcomponent(modules = [SubModule::class])
interface SubInjector1 {

    fun getService1() : Service1

    val subInjector2Builder : SubInjector2.Builder

}