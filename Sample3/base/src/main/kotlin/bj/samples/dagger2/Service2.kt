package bj.samples.dagger2

import javax.inject.Inject

class Service2(private val dependency: Service){
    init {
        println("I'm service 2!")
    }

    fun show() { dependency.show() }
}