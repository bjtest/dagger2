package bj.samples.dagger2

import javax.inject.Inject

class Service {
    init {
        println("hello world")
    }

    fun show() {
        println("show message")
    }
}
