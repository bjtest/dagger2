package bj.samples.dagger2

import java.time.LocalDateTime
import javax.inject.Inject
import javax.inject.Singleton

class Service1 : ServiceInterface{
    init {
        println("service1 start time: ${LocalDateTime.now()}")
    }

    override fun show() { println("service1 show!") }
}
