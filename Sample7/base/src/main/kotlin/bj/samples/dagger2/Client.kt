package bj.samples.dagger2

import javax.inject.Inject

class Client {
    @Inject
    lateinit var service: ServiceInterface

    fun show() { service.show() }
}