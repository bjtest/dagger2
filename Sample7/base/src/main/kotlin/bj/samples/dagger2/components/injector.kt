package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.modules.BaseModule
import dagger.Component

@Component(modules = [BaseModule::class])
interface Injector {

    fun inject(client: Client): Client

}