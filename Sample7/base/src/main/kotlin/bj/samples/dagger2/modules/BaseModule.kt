package bj.samples.dagger2.modules

import bj.samples.dagger2.Service1
import bj.samples.dagger2.ServiceInterface
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class BaseModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideService(): Service1 = Service1()
    }

    @Binds
    abstract fun provideServiceImp( serviceImp: Service1) : ServiceInterface

}