package bj.samples.dagger2

import bj.samples.dagger2.components.DaggerInjector


fun main(args: Array<String>) {

    val client = Client()

    DaggerInjector.create().inject(client).show()

}