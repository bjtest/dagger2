package bj.samples.dagger2.components

import bj.samples.dagger2.Client
import bj.samples.dagger2.modules.BaseModule
import dagger.Component
import javax.inject.Named

@Component(modules = [BaseModule::class])
interface Injector {

    fun getStringMap(): Map<Int,String>

}