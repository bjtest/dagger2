package bj.samples.dagger2

import bj.samples.dagger2.components.DaggerInjector


fun main(args: Array<String>) {

    val injector = DaggerInjector.create()
    println( "value1:${injector.getStringMap().get(1)},value2:${injector.getStringMap().get(2)}" )

}