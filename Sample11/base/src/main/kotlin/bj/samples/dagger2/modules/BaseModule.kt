package bj.samples.dagger2.modules

import dagger.Module
import dagger.Provides
import dagger.multibindings.ElementsIntoSet
import dagger.multibindings.IntKey
import dagger.multibindings.IntoMap
import dagger.multibindings.IntoSet
import javax.inject.Named

@Module
object BaseModule {

    @Provides
    @IntKey(1)
    @IntoMap
    @JvmStatic
    fun provideString1(): String = "ABC"

    @Provides
    @IntKey(2)
    @IntoMap
    @JvmStatic
    fun provideString2(): String = "DEF"


}